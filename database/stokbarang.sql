-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2021 at 04:08 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `utspbw`
--

-- --------------------------------------------------------

--
-- Table structure for table `stokbarang`
--

CREATE TABLE `stokbarang` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_brg` varchar(50) CHARACTER SET utf16 COLLATE utf16_bin NOT NULL,
  `kode_brg` varchar(5) NOT NULL,
  `stok_brg` varchar(10) NOT NULL,
  `harga` varchar(20) NOT NULL,
  `pemasok` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stokbarang`
--

INSERT INTO `stokbarang` (`id`, `nama_brg`, `kode_brg`, `stok_brg`, `harga`, `pemasok`) VALUES
(0, 'Kopi Luwak', 'KL850', '300', '5000', 'PT Serba Bisa'),
(1, 'Pepsi Red', 'PR021', '853', '6500', 'PT Indofood'),
(2, 'Wafer Howland', 'WH728', '1050', '8400', 'PT Serba Bisa'),
(3, 'Yogurt Chimera \"Blueberry\"', 'YC041', '5350', '6000', 'PT Dairy Indonesia');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stokbarang`
--
ALTER TABLE `stokbarang`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stokbarang`
--
ALTER TABLE `stokbarang`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
